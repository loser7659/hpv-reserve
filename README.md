# 前提准备

```tex
前提准备:
1.疫苗信息源:公众号-约苗
2.通过抓包获取wxtoken: 详情看图1
3.在家庭成员中添加好个人信息
4.测试Python版本: 3.8.10
```

![[Pasted image 20211117113336.png]]
## 需要修改的信息
```tex
需要修改的一些信息
1.除了wxtoken之外还需要修改自己的一些地区信息和疫苗信息

重点修改如下几项即可:
 'regionCode': 3701, #区域码 可根据自己抓包数据修改
 'longitude': '117.11876678466797', #经度 (这两项根据自己区域修改 可不填)
 'latitude': '36.650978088378906', #纬度
 'customId': 2 # customId等于2是四价 等于52是两价 3为九价

具体参数根据自己抓包获取的信息修改(参考图2) 经纬度这个信息自行去地图软件获取或留空
```
![[Pasted image 20211117114416.png]]


# 部分接口信息
```tex
#获取附近有疫苗的医院信息
http://wx.scmttec.com/base/department/getDepartments.do

#根据工作日筛选
https://wx.scmttec.com/order/subscribe/workDays.do

#根据时间筛选医院
https://wx.scmttec.com/order/subscribe/findSubscribeAmountByDays.do
```

# 代码
## 请求头
headers 头部信息:
```python
#cookie这个位置也可以根据自己的抓包信息来修改

tk = f.readline()

cookie = 'UM_distinctid=17700ac42240-0b33a11b9e4d68-20582e26-1fa400-17700ac42295b4; _xzkj_= ' + tk + '; _xxhm_=%7B%22address%22%3A%22%E5%AE%89%E4%B9%90%E9%95%87%E5%B0%8F%E5%8C%BA%22%2C%22awardPoints%22%3A0%2C%22birthday%22%3A750096000000%2C%22createTime%22%3A1589854819000%2C%22headerImg%22%3A%22http%3A%2F%2Fthirdwx.qlogo.cn%2Fmmopen%2FajNVdqHZLLD2pMRmmYaQaLBDChem66aoN9svAnicpZPDx2jP21aPaPtCHBljnfwxSu9Gwia5NBAUH1tj7xa4FbVA%2F132%22%2C%22id%22%3A5547063%2C%22idCardNo%22%3A%2237010519931009501X%22%2C%22isRegisterHistory%22%3A0%2C%22latitude%22%3A36.650768%2C%22longitude%22%3A117.118713%2C%22mobile%22%3A%2215066121027%22%2C%22modifyTime%22%3A1606914586000%2C%22name%22%3A%22%E8%B0%A2%E8%B6%85%22%2C%22nickName%22%3A%22%E1%83%A6Mr%E3%80%86%C2%B0%E8%B0%A2%22%2C%22openId%22%3A%22oWzsq5_KzifgG9-xnWt65oBLhRcc%22%2C%22regionCode%22%3A%22370105%22%2C%22registerTime%22%3A1606914586000%2C%22sex%22%3A1%2C%22source%22%3A1%2C%22uFrom%22%3A%22depa_vacc_detail%22%2C%22unionid%22%3A%22oiGJM6OGsfxJNteI_rfIzkf3Jh7k%22%2C%22wxSubscribed%22%3A1%2C%22yn%22%3A1%7D; CNZZDATA1261985103=596264046-1610954889-%7C1611020154'


headers = {

 'Cookie': cookie,
 'tk': tk

}
```

## 请求参数
param 请求参数:
```python

#这里就是我们上面需要修改的一些信息(根据自己情况修改)
param = {'offset': '0',

 'limit': '1000',

 'name': '',

 'regionCode': 3701, #区域码 可根据自己抓包数据修改

 'isOpen': 1,

 'longitude': '117.11876678466797', #经度 (这两项根据自己区域修改 可不填)

 'latitude': '36.650978088378906', #纬度

 'sortType': 1,

 'vaccineCode': '',

 'customId': 2
		} # customId等于2是四价 等于52是两价 3为九价
```

## 所需包
```python 
import requests

import json

import time

import datetime

import tkinter

import tkinter.messagebox
```

##  运行脚本 
```python
python main.py
```

![image-20211119125244677](C:\Users\S\AppData\Roaming\Typora\typora-user-images\image-20211119125244677.png)